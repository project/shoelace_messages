/**
 * @file shoelace_messages.common.js
 *
 * Javascript for integrating the shoelace Alert into drupal
 * Replaces the standard drupal status messages and creates shoelace alerts instead
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.shoelace_messages = {};

  // This is a unique string with format "drupal-[module name]"
  Drupal.shoelaceMessagesUnique = 'drupal-shoelace-messages';
  
  // The targeted element for the context to work in
  Drupal.shoelaceMessagesContext = 'body';

  /**
   * Registers behaviours
   */
  Drupal.behaviors.shoelace_messages = {
    attach: function (context, settings) {

      // Code inside this block runs once (only on initial page load)
      $(Drupal.shoelaceMessagesContext, context).once(Drupal.shoelaceMessagesUnique).each(function () { 
        
        // Creating shoelace messages on page request (html request)
        if (typeof settings.shoelace_messages !== 'undefined' && typeof settings.shoelace_messages.messages_array !== 'undefined') {
          Drupal.shoelace_messages.createMessages(settings.shoelace_messages.messages_array);
          settings.shoelace_messages.messages_array = null;
        }
        
      });

    }
  };

  // creating shoelace messages on ajax requests
  Drupal.AjaxCommands.prototype.shoelaceMessagesCommand = function (ajax, response, status) {
    Drupal.shoelace_messages.createMessages(response.messages_array);
  };

  // Create and display shoelace status messages
  Drupal.shoelace_messages.createMessages = function (messages_array) {

    var previousAlertsList = document.querySelectorAll('sl-alert');
    previousAlertsList.forEach(function(alertItem) {
      alertItem.remove();
    });
    
    // Defaults
    const type = 'primary';
    const closable = true;
    const icon = 'info-circle';
    const duration = '5000';
    
    if (Array.isArray(messages_array) && messages_array.length) {
      
      let _icon = icon;
      let _duration = duration;

      for (let i in messages_array) {

        if(messages_array[i].type == 'status' || messages_array[i].type == 'success') {
          messages_array[i].type = 'success';
          _icon = (messages_array[i].icon) ? messages_array[i].icon : 'check2-circle';
        }
        else if (messages_array[i].type == 'error' || messages_array[i].type == 'danger') {
          messages_array[i].type = 'danger';
          _icon = (messages_array[i].icon) ? messages_array[i].icon : 'exclamation-octagon';
          _duration = (messages_array[i].duration === undefined || messages_array[i].duration === null) ? 10000 : messages_array[i].duration;
        }
        else {
          messages_array[i].type = (messages_array[i].type!='warning') ? type : messages_array[i].type;
          _icon = (messages_array[i].icon) ? messages_array[i].icon : 'exclamation-triangle';
          _duration = (messages_array[i].duration === undefined || messages_array[i].duration === null) ? 10000 : messages_array[i].duration;
        }

        Drupal.shoelace_messages.notify(
          messages_array[i].message, 
          messages_array[i].type, 
          (messages_array[i].closable === undefined || messages_array[i].closable === null) ? closable : messages_array[i].closable, 
          _icon, 
          _duration,
        );
      }

    }

  }
  
  // Emit shoelace messages as toast notifications
  Drupal.shoelace_messages.notify = function (message, type = 'primary', closable = true, icon = 'info-circle', duration = 'Infinity') {
    const alert = Object.assign(document.createElement('sl-alert'), {
      type: type,
      closable: closable,
      duration: duration,
      innerHTML: `
        <sl-icon name="${icon}" slot="icon"></sl-icon>
        ${message}
      `
    });

    document.body.append(alert);
    return alert.toast();
  }

}(jQuery, Drupal, drupalSettings));
