<?php

/**
 * @file
 * Contains shoelace_messages.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_help().
 */
function shoelace_messages_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the shoelace_messages module.
    case 'help.page.shoelace_messages':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides floating popup status messages using the Shoelace Alert component') . '</p>';
      $output .= '<p>' . t('This module replaces the standard drupal status messages and creates shoelace alerts instead') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_page_attachments_alter().
 */
function shoelace_messages_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'shoelace_messages/common';
}

/**
 * Implements hook_preprocess().
 *
 * Formats the message_list array and sends it to javascript
 * for shoelace to create the messages .
 */
function shoelace_messages_preprocess_status_messages(&$variables) {

  $messages_array = [];

  if (isset($variables['message_list']) && !empty($variables['message_list'])) {

    foreach ($variables['message_list'] as $type => $messages) {
      if (is_array($messages)) {
        foreach ($messages as $index => $message) {
          $messages_array[] = [
            'type' => $type,
            'message' => ($message instanceof Markup) ? $message->__toString() : \Drupal::service('renderer')->render($message),
          ];
        }
      }
      else {
        $messages_array[] = [
          'type' => $type,
          'message' => $messages,
        ];
      }
    }

    // Using temp store.
    $tempstore = \Drupal::service('tempstore.private')->get('shoelace_messages');

    // Set a tempstore variable for later use during ajax requests,.
    $tempstore->set('messages_array', $messages_array);

    // Send the messages array to Js for shoelace to create the messages.
    // - this line doesn't send the variable to js during ajax requests
    // hence why we are setting a tempstore above for ajax requests.
    $variables['#attached']['drupalSettings']['shoelace_messages']['messages_array'] = $messages_array;

    /* Remove standard status messages (but not for super admin user) */
    $uid = \Drupal::currentUser()->id();
    if ($uid != 1) {
      unset($variables['message_list']);
    }

  }

}
