CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
This module adds great looking status messages that stay well away 
from your design by being absolutly positioned while beautifully 
alerting your users temporarily before they say byebye and leave you be!

Built using the wonderful js library Shoelace and using the Alert component.

This setup greatly enhances the page since these messages are isolated 
from the layout and do not intefere with the design. The standard status 
messages are part of the page, causing them to move or push page elements 
further down, under the fold, which can disrupt and clutter the design 
for users. Not good and very annoying!

This module replaces the standard drupal status messages and creates 
shoelace alerts instead. This kind of messages are also known 
as "Toast" notifications (or alerts).

Works for all status messages including messages created during Ajax requests.

This module is a perfect extension for the Drupal Commerce module. 
It was first developed for a Drupal Commerce website.

** Note: Standard messages WILL SHOW for superuser (uid: 1). 
Can be changed in shoelace_messages.module (on line 75)

REQUIREMENTS
------------
This module does not have any dependency on any other module.
Shoelace (loaded via cdn)


INSTALLATION
------------
composer require drupal/shoelace_messages

Or, Install as normal (see http://drupal.org/documentation/install/modules-themes).


CONFIGURATION
-------------
No setup required (for now)
Just install the module and let it do its thing.


MAINTAINERS
-----------
hga77: https://www.drupal.org/u/hga77
