<?php

namespace Drupal\shoelace_messages\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command to create shoelace messages from an array of status messages.
 */
class ShoelaceMessagesCommand implements CommandInterface {

  /**
   * The messages.
   *
   * @var array
   */
  protected $messagesArray;

  /**
   * {@inheritdoc}
   */
  public function __construct($messagesArray = NULL) {
    $this->messagesArray = $messagesArray;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'shoelaceMessagesCommand',
      'messages_array' => $this->messagesArray,
    ];
  }

}
