<?php

namespace Drupal\shoelace_messages\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Drupal\shoelace_messages\Ajax\ShoelaceMessagesCommand;

/**
 * Event Subscriber MessagesEventSubscriber.
 */
class MessagesEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    return [
      KernelEvents::RESPONSE => [['onKernelResponse']],
    ];

  }

  /**
   * Handles onKernelResponse event and adds the shoelace messages on the page.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The response event.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();

    // Using private temp store to get the status messages provided by the
    // mymodule_preprocess_status_messages.
    $tempstore = \Drupal::service('tempstore.private')->get('shoelace_messages');

    // Only for AJAX responses. Cancel if not ajax.
    if (!$response instanceof AjaxResponse) {
      // Delete tempstore variable.
      $tempstore->delete('messages_array');
      return;
    }

    // Array of messages.
    $messagesArray = $tempstore->get('messages_array');

    // Have we set messages to be displayed?
    if (isset($messagesArray) && !empty($messagesArray)) {

      // Yes we have messages, so let's call the javascript command function
      // to display them.
      $response->addCommand(
        new ShoelaceMessagesCommand($messagesArray)
      );

      // Delete tempstore variable.
      $tempstore->delete('messages_array');

    }

    // Let's do this...
    $event->setResponse($response);
  }

}
